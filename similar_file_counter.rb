require 'optparse'

options = {}
OptionParser.new do |opts|
  opts.banner = "Usage: similar_file_counter.rb [options]"

  opts.on('-n', '--path PATH', 'Your path') do |path|
    options[:path] = path
  end
end.parse!

path = options[:path] || '/'
puts "Scanning #{path}..."

list_file = Dir.glob(path + "/*").select { |e| File.file? e }
puts "Found #{list_file.count} file(s)"

hash_file = {}

list_file.each do |file|
    content = File.read(file)

    if hash_file[content] == nil
        hash_file[content] = 1
    else
        hash_file[content] += 1
    end
end

highest_value = hash_file.values.max
puts "#{hash_file.key(highest_value)} #{highest_value}"